APP_PATH="$GOPATH/src/bitbucket.org/abemedia/social-count/"
APP_FILE="$GOPATH/bin/social-count"

source ~/Applications/bashutils/library/version.sh

APP_ROOT="$(pwd)"
OLD_VERSION=$(<$APP_ROOT/env/OPENSHIFT_SOCIAL_COUNT_VERSION)
export APP_VERSION=$(increment_version "$OLD_VERSION")

# update manifest
erb "$APP_ROOT/metadata/.manifest.yml.erb" > "$APP_ROOT/metadata/manifest.yml"

# build new binary
mkdir -p "$APP_ROOT/versions/$APP_VERSION"
cd $APP_PATH
go install
cp "$APP_FILE" "$APP_ROOT/versions/$APP_VERSION/"

# update version variable
echo "$APP_VERSION" > "$APP_ROOT/env/OPENSHIFT_SOCIAL_COUNT_VERSION"

# remove old version
rm -rf "$APP_ROOT/versions/$OLD_VERSION/"

# ouput result
echo "$OLD_VERSION -> $APP_VERSION"

cd "$APP_ROOT"